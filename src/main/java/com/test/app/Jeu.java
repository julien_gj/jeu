import java.util.Scanner;
import java.util.concurrent.*;
public class Jeu {
	
	public static void main(String[] args) {
			Scanner lecteur = new Scanner(System.in); 
			System.out.println("Donner les bornes: ");
			int minimum = lecteur.nextInt(); 
			int maximum = lecteur.nextInt() ;
			PereFouras N = new PereFouras(minimum,maximum) ;
			System.out.println(N.getMystere());
			System.out.println("Essayer");
			int essai = lecteur.nextInt(); 	
			int nbEssai = 1 ;
		while(N.estDifferent(essai)) {
			System.out.println("Réessayer");
			essai = lecteur.nextInt(); 
			++nbEssai;
		}
		lecteur.close();
		System.out.println(nbEssai+" essais");
}
}
